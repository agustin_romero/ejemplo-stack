var app = angular.module('films', [ "ngResource" ]);

app.controller('FilmsController', [ '$scope', '$http',
                                     
	function($scope, $http) {
				$scope.film = {
				title : "",
				year : "",
				director : ""
			}
		$scope.getFilms = function() {
			$http.get('/films').success(function(data) {
				$scope.films = data;
			});
		}
		
		$scope.addFilm = function() {
			$http.post('/films', $scope.film).success(function(data) {
				$scope.msg = 'Pelicula creada correctamente';
				$scope.getFilms();
			}).error(function(data) {
				$scope.msg = 'Se ha producido un error';
			});
		}
} ]);