package com.autentia.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.autentia.dao.FilmDAO;
import com.autentica.entidades.Film;

public class FilmServiceImpl implements FilmService{
	@Autowired
	private FilmDAO filmDAO; 
	
	@Override
	public List<Film> getFilms() {
		return filmDAO.getFilms();
	}

	@Override
	public void addFilm(Film film) {
		filmDAO.addFilm(film);
		
	}

}
