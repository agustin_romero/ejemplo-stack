package com.autentia.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.autentica.entidades.Film;

public interface FilmService{
	@Autowired
	public List<Film> getFilms();
	@Autowired
	public void addFilm(Film film);
}
