package com.autentia.dao;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import com.autentica.entidades.Film;
@Repository
public class FilmDAOImpl implements FilmDAO{

	ApplicationContext ctx = new GenericXmlApplicationContext("SpringConfig.xml");
	MongoOperations mongoOperations= (MongoOperations) ctx.getBean("mongoTemplate");
	
	@Override
	public List<Film> getFilms() {
		return mongoOperations.findAll(Film.class);
		
	}

	@Override
	public void addFilm(Film film) {
		mongoOperations.save(film);		
	}

}
