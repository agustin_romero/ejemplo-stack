package com.autentia.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.autentica.entidades.Film;


public interface FilmDAO {
	
	public List<Film> getFilms();
	
	public void addFilm(Film film);
	
}
